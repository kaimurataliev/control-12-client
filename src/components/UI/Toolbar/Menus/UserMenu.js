import React, {Fragment} from 'react';
import {MenuItem, Nav, NavDropdown, Thumbnail} from "react-bootstrap";

const UserMenu = ({user, logout}) => {
    const navTitle = (
        <Fragment>
            Hello, <b>{user.username}</b>!
        </Fragment>
    );

    return (
        <Nav pullRight>
            <NavDropdown title={navTitle} id="user-menu">
                <MenuItem onClick={logout}>Logout</MenuItem>
                <Thumbnail href="#" alt="50x50" src={user.avatar} />
            </NavDropdown>
        </Nav>
    )
};

export default UserMenu;
